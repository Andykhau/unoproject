// 2037941 ANDY KHAU
//CREATE A CONSTANT FOR THE CARD VALUE
//convert my string to numbers
public enum Value
{
	ZERO { 
		public String toString(){
			return "0";
		}
	},
	ONE { 
		public String toString(){
			return "1";
		}
	},
	TWO { 
		public String toString(){
			return "2";
		}
	},
	THREE{ 
		public String toString(){
			return "3";
		}
	},
	FOUR{ 
		public String toString(){
			return "4";
		}
	},
	FIVE{ 
		public String toString(){
			return "5";
		}
	},
	SIX{ 
		public String toString(){
			return "6";
		}
	},
	SEVEN { 
		public String toString(){
			return "7";
		}
	},
	EIGTH { 
		public String toString(){
			return "8";
		}
	},
	NINE { 
		public String toString(){
			return "9";
		}
	},
	DRAW2 { 
		public String toString() {
			return "+2";
		}
	}
}