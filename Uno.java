import java.util.Scanner;

import javax.lang.model.util.ElementScanner6;

public class Uno
{	
	//Scanner
	public static Scanner scan = new Scanner(System.in);
	public static Deck deck;
	public static PileOfCards pile;
	public static Card topCard;
	public static Player playerOne = new Player();
	public static Player playerTwo = new Player();
	public static boolean roundFinish = false;
	public static boolean turnFinish = false;
	public static boolean gameOn = true;
	public static boolean playingAgain;
	public static int intInput = 0;
	public static String choiceInput = "";
	
	public static void main (String [] args)
	{	
		while(gameOn)
		{
			playerOne = new Player();
			playerTwo = new Player();
		
			gameintro();
			
			while(!roundFinish)
			{
				roundTurn(playerOne);
				if(roundFinish)
				{
					break;
				}
				roundTurn(playerTwo);
				if(roundFinish)
				{
					break;
				}
			}

			playingAgain = playAgain();
			if(playingAgain)
			{	
				System.out.println("Loading Changes . . .");
				System.out.println("Game has been successfulling restarted");
				continue;
			}
			else if(!playingAgain)
			{
				gameOn = false;
				break;
			}
		}
	}

	public static boolean playAgain()
	{
		System.out.println("Would you like to play again y for yes n for no");
		choiceInput = scan.nextLine();
		boolean choiceValid = false;
		boolean result = false;
		
		while(!choiceValid){
			if (choiceInput.equals("n") || choiceInput.equals("N"))
			{
				System.out.println("Thank you for playing please come again");
				result = false;
				choiceValid = true;
				break;
			}
			else if (choiceInput.equals("y") || choiceInput.equals("Y"))
			{
				System.out.println("Thank you for enjoying game, it will restart shortly");
				result = true;
				choiceValid = true;
				break;
			}
			else
			{
				System.out.println("Input is invalid please enter y or n");
				choiceInput = scan.nextLine();
				continue;
			}
		}
		return result;
	}

	public static void roundTurn(Player player)
	{
		while(!turnFinish)
		{
			playerTurn(playerOne);
			break;
		}
		
		//check is player has 0 cards
		boolean win = checkWin(player);
		turnFinish = false;
		if(win)
		{
			roundFinish = true;
		}
		else if(!win)
		{
			roundFinish = false;
		}
	}
	public static void playerTurn(Player player)
	{
		//Print the pile card to remind player
		System.out.println("top card is: "+ topCard);
					
		//Print player's hand
		System.out.println(player.toString());
		
		//Choose what to do draw or play card
		System.out.println("p to play a card and d to draw a card");
		choiceInput = scan.nextLine();
		boolean choiceValid = false;
		
		//if play card
		while(!choiceValid)
		{
			if(choiceInput.equals("p") || choiceInput.equals("P"))
			{
				//choose Card that matches the pile card either number or color
				intInput = Integer.parseInt(scan.nextLine());
				boolean inputValid = false;
				while(!inputValid)
				{
					//if card is plus two check if match then run plusTwo();
					if(topCard.getColor() == player.pickACard(intInput).getColor() && player.pickACard(intInput).getValue() == Value.DRAW2)
					{
						plusTwo(player);
						topCard = player.pickACard(intInput);
						player.removeACard(intInput);
						pile.addCard(topCard);
						inputValid=true;
						break;
					}
					//if card is not special then check if match then run playTurn
					else if(topCard.getColor() == player.pickACard(intInput).getColor() || topCard.getValue() == player.pickACard(intInput).getValue())
					{								
						topCard = player.pickACard(intInput);
						player.removeACard(intInput);
						pile.addCard(topCard);
						inputValid=true;
						break;
					}
					else
					{
						System.out.println("invalid card");
						System.out.println("p to play a card and d to draw a card");
						intInput = Integer.parseInt(scan.nextLine());
						continue;
					}
				}
			}
			//if draw
			else if(choiceInput.equals("d") || choiceInput.equals("D"))
			{
			//run method drawTurn(player); Then skip to next player
				drawTurn(player);
				System.out.println("You draw a card your turn has ended.");
			}
			else
			{
				System.out.println("Invalid input");
				System.out.println("p to play a card and d to draw a card");
				choiceInput = scan.nextLine();
				continue;
			}
			choiceValid = true;
			break;
		}
		turnFinish = true;
	}
	
	public static void gameintro()
	{
		//Initializing Global Variables
		pile = new PileOfCards();
		deck = new Deck();

		//Intro Messages
		System.out.println("Welcome to Uno");

		//Shuffling Deck
		deck.shuffle();
		deck.shuffle();
		deck.shuffle();
		deck.shuffle();

		//Drawing 5 cards for all players
		drawPlayerCards();

		//Setting first card
		settingFirstCardOnPile();
	}
		
		public static void drawPlayerCards()
	{
		Card card;
		for(int i = 0; i < 5; i++)
		{
			card = deck.draw();
			playerOne.addCardsToHand(card);
			card = deck.draw();
			playerTwo.addCardsToHand(card);
		}
		
	}
	
	public static void settingFirstCardOnPile()
	{
		Card card = deck.draw();
		pile.addCard(card);
		topCard = card;
	}
	
	public static void drawTurn(Player player)
	{
		Card card = deck.draw();
		player.addCardsToHand(card);
	}
	
	public static void plusTwo(Player player)
	{
		drawTurn(player);
		drawTurn(player);
	}
	
	public static boolean checkWin(Player player)
	{
		//Check if get length if equals 0 then you win 
		if(player.handLength() == 0)
		{
			// if player won run endGame();
			System.out.println(player + "Win the game!");
			return true;
		}
		else
		{
			return false;
		}
	}
}