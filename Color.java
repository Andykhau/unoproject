/*
2037941 ANDY KHAU
CONSTATN CREATION FOR THE COLORE CARDS 
*/
public enum Color
{	
	RED {
		public String toString(){
			return "\u001B[31m RED \u001B[31m";
		}
		},
	BLUE {
		public String toString(){
			return "\u001B[34m BLUE \u001B[34m";
		}
		},
	YELLOW {
		public String toString(){
			return "\u001B[33m YELLOW \u001B[33m";
		}
		},
	GREEN {
		public String toString(){
			return "\u001B[32m GREEN \u001B[32m";
		}
		}
}