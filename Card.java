//2037941 ANDY KHAU
//CARD CLASS OBJECT
//CREATION OF ONE UNO CARD
public class Card
{
	//fields
	public final Color color;
	public final Value value;
	
	//CONSTRUCTOR to set default values
	public Card(Color color, Value value)
	{
		this.color = color;
		this.value = value;
	}
	//TOSTRING METHOD TO DEFINE EACH OF CARDS
	public String toString()
	{
		String card = "|" + this.color + "| " + this.value + " |" ;
		return card;
	}
	
	public Color getColor()
	{
		return this.color;
	}
	
	public Value getValue()
	{
		return this.value;
	}
}