/*
2037941 ANDY KHAU
DECK CLASS OBJECT
creation of all the UNO CARDS
*/
public class Deck
{
	//PRIVATE FIELDS
	private dynamicCardArray Cards;
	
	//CONSTRUCTOR FOR THE PRIVATE FIELDS
	public Deck() 
	{
		//calling the dynamicCardArray class 
		this.Cards = new dynamicCardArray();
		
		//printing the UNO cards
		for(Color newColor: Color.values())
		{
			for(Value newValue: Value.values())
			{
				for(int i = 0; i < 4; i++)
				{
					Card card = new Card(newColor,newValue);
					this.Cards.addCard(card);
				}
			}
		}
	}
	
	//get the shuffle() method 
	public void shuffle()
	{
		 this.Cards.shuffle();
	}
	
	// reshuffle/reset the deck method. TAKES THE DISCARD PILE AND RESHUFFLE IT.
	//public void reshuffle()
	
	public Card draw(){
		Card firstCard = this.Cards.getCardFromTop();
		return firstCard;
	}
		
	
	//toString - TO PRINT MY CARDS
	public String toString()
	{
		String returnString = "";
		for(int i=0; i < Cards.length(); i++)
		{
			Card tempCard = this.Cards.getCard(i);
			returnString = returnString + tempCard + "\n";
		}
		return returnString;
	}
}