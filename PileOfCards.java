public class PileOfCards
{
	private dynamicCardArray cardPile;
	
	//constructor
	public PileOfCards()
	{
		this.cardPile = new dynamicCardArray();
	}
	
	//ADD CARD ON THE TOP OF THE PILE
	public void addCard(Card card)
	{
		this.cardPile.addCard(card);
	}

}