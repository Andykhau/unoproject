// 2037941 ANDY KHAU 
// UNO PLAYER DECISIONS
public class Player
{
	//FIELDS
	
	private dynamicCardArray playerHand;
	
	
	//CONSTRUCTOR
	public Player()
	{

		this.playerHand = new dynamicCardArray();
	}
	
	public int handLength()
	{
		return playerHand.length();
	}

	//ADD CARDS TO HAND - SET METHOD
	public void addCardsToHand(Card card)
	{
		this.playerHand.addCard(card);
	}
	//PICK A CARD - GET METHOD 
	public Card pickACard(int index)
	{
		return this.playerHand.getCard(index);
	}

	//REMOVE A CARD
	public void removeACard(int index){
		this.playerHand.remove(index);
	}
	
	// public Card playACard(int i)
	// {
        // Card temp = this.playerHand.get(i-1);
        // this.playerHand.remove(i-1);
        // return temp;
    // }
	
	//HOW MANY CARD IN HAND - GET METHOD
	public int howManyCardsInHand()
	{
		return this.playerHand.length();
	}
	//DISPLAY THE CARD IN YOUR HAND - SET METHOD
	 public String toString()
	{
		String print = "Your hand contains: ";
		for(int i = 0; i < this.playerHand.length(); i++){
			if(i < this.playerHand.length() - 1){
				print += this.playerHand.getCard(i) + ", ";
			}
			else if(i == this.playerHand.length() - 1){
				print += this.playerHand.getCard(i);
			}
		}
		return print;
	}
	
}