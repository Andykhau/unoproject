/*
2037941 ANDY KHAU
dynamicCardArray CLASS OBJECT

*/
import java.util.Random;
public class dynamicCardArray
{
	//PRIVATE FIELDS 
	private Card[] unoCards;
	private int length;
	
	//CONSTRUCTOR THAT WILL HAVE 
	public dynamicCardArray()
	{
		this.unoCards = new Card[250];
		this.length = 0;
	}
	
	//SHUFFLE METHOD - TAKE tWO CARD AND CHANGE ITS LOCATION FOR HOW MANY CARDS YOU HAVE IN YOUR DECK.
	public void shuffle()
	{
		Random rand = new Random();
		
		for(int i = 0; i < length;i++)
		{
			swap(i, rand.nextInt(this.length));
		}
	}
	
	//SWAP() - SWAP POSITION OF CARD NUMBER 1 TO CARD NUMBER 2 WITH THE HELP OF RANDOM THAT SELECT THE NEEDED CARD TO SWAP THE CARD ON REPEAT.
	public void swap(int cardPositionOne, int cardPositionTwo)
	{
		Card tempCard = this.unoCards[cardPositionTwo];
		this.unoCards[cardPositionTwo] = this.unoCards[cardPositionOne];
		this.unoCards[cardPositionOne] = tempCard;
	}
	
	//dynamicCardArray METHODS
	// length method. have the length of the cards in hands
	public int length()
	{
		return this.length;
	}
	
	//add card method. add cards to the array
	public void addCard(Card card)
	{
		this.unoCards[this.length] = card;
		this.length++;
	}
	
	//it removes a card for the dynamiccardarray
    public void remove(int i){
        if(i > this.length){
            throw new ArrayIndexOutOfBoundsException("no value");
        }
        else{
            for(int j = i; j < this.length; j++){
            this.unoCards[j] = this.unoCards[j+1];
            }
            this.length--;
        }
    }
	//getCArd
	public Card getCard(int i)
	{
		return this.unoCards[i];
	}
	
	//toString - TO PRINT MY CARDS
	public String toString()
	{
		String returnString = " ";
		
		for(int i = 0; i < length; i++)
		{
			returnString += this.unoCards[i];
			returnString += ", ";
		}
		
		return returnString;
	}

	 //checks if your card you are looking for is amongst the cards available
	 public boolean contains(Card s)
	 {
		 for(int i = 0; i < this.length; i++){
			 if(this.unoCards[i] == (s))
			 {
				return true; 
			 }
		 }
		 return false;
	 }

	//removes a card from the top of the deck or index 0 basically drawing a card
    public Card getCardFromTop(){
        Card FirstCard = this.unoCards[0];
        remove(0); 
        return FirstCard;
    }
	
	//insert inserts a card before the index inserted that shifts everything to the right one
    public void insert(int i, Card inserted){
        if(i > this.length){
            throw new ArrayIndexOutOfBoundsException("no value");
        }
        else{
            for(int j = this.length-1; j >= i; j--){
                this.unoCards[j+1] = this.unoCards[j];
            }
            this.unoCards[i] = inserted;
            this.length++;
        }
    }
	
	// //remove card from hands
	// public Card rm(int index)
	// {
		// Card temp = this.unoCards[index];	
		// remove(index);
		// return temp;
		
	// }
	
}
